const express = require('express');

const heapSnapshot = require('./src/heap-snapshot');
const requestLog = require('./src/request-log');

const app = express();

app.use('/heap-snapshot', heapSnapshot);
app.use('/request-log', requestLog);

app.listen(3000);
