const express = require('express');
const heapdump = require('heapdump');
const fs = require('fs');

const router = express.Router();

router.get('/', (req, res) => {
  const now = new Date;
  const fl = `snapshot_${now.toJSON()}.heapsnapshot`;
  heapdump.writeSnapshot(fl, (err, filename) => {
    res.download(filename, err => fs.unlink(filename));
  });
});

module.exports = router;
