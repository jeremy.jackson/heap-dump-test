const express = require('express');

const router = express.Router();

const requestLog = [];

router.get('/', (req, res) => {
  requestLog.push({ url: req.url, date: new Date() });
  res.send(JSON.stringify(requestLog));
});

module.exports = router;
